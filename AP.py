class AP:
    def __init__(self, ap_type, x, y, color):
        self.type = ap_type  # lifi or wifi
        self.x = x
        self.y = y
        self.my_id = None
        self.connected_users = []
        self.users_their_distance = {}  # all users
        self.color = color
        self.average_payoff = 0
        self.lagr_multiplier_w = None   # 3 is best value
        # self.lambd = None
        self.omega = 0
        self.m = 0
        self.num_connected_users = 0  # m alpha 1
        self.m_alpha = 0              # m alpha
        self.users_their_sinrs = {}

    def get_ap_sum_rate(self):
        sum_rate = 0
        for user in self.connected_users:
            sum_rate += user.my_rate
        return sum_rate/10**6