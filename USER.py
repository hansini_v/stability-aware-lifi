class User:
    def __init__(self, x, y, required_rate):
        self.x = x
        self.y = y
        # for mobility
        self.xd = x
        self.yd = y
        self.x_curr = x
        self.y_curr = y
        # Save the positions
        self.x_mobility = []
        self.y_mobility = []
        self.my_id = None
        self.my_sinr = 0 # fixme
        self.my_sinr_dict = {}
        self.my_ap = None
        self.my_rate = -1
        self.my_qos = None
        self.achievable_rate = -1
        self.achievable_rate_dict = {}
        self.aps_their_distance = {}
        self.aps_their_sinrs = {}
        self.best_lifi_sinr_and_ap = [0, ]
        self.best_wifi_snr_and_ap = [0, ]
        self.connected = False
        self.blocked = False
        self.possible_aps = []
        self.my_resources = 0
        self.my_payoff = 0
        self.mutation_prob = 0
        self.k_opt = 1
        self.g_opt = None
        self.demand = required_rate
        self.achiev_rate_ap_id = {}
        self.color = None
        self.num_vho = 0
        self.num_hho = 0
        self.elevation_angle = 0
        self.my_payoff_list = []
        self.my_blockages = []
        self.made_ho = False
        self.blockage_duration = 0