import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import matplotlib.patches as mpatches
ip_addresses = ['172.16.1.1', '172.16.1.2']
pd.options.display.float_format = '{:.6f}'.format
count = 0
# info: 3 or 5 sec!!!
how_often_switch = 3
runs = [10, 20, 101, 201]
for bw in runs:
    if bw == 10:
        color = 'blue'
        ls = '-'
        user_lifi = pd.read_csv("captures/0.final_1h_10M_fq10M_nodelay/user_lifi.csv", index_col='No.')
        user_wifi = pd.read_csv("captures/0.final_1h_10M_fq10M_nodelay/user_wifi.csv", index_col='No.')

    elif bw == 20:
        color = 'green'
        ls = '-'
        user_lifi = pd.read_csv("captures/0.final_1h_20M_fq20M_nodelay/user_lifi.csv", index_col='No.')
        user_wifi = pd.read_csv("captures/0.final_1h_20M_fq20M_nodelay/user_wifi.csv", index_col='No.')
    elif bw == 101:
        color = 'blue'
        ls =':'
        user_lifi = pd.read_csv("captures/0.day2_10M/user_lifi.csv", index_col='No.')
        user_wifi = pd.read_csv("captures/0.day2_10M/user_wifi.csv", index_col='No.')
    elif bw == 201:
        color = 'green'
        ls = ':'
        user_lifi = pd.read_csv("captures/0.day2_20M/user_lifi.csv", index_col='No.')
        user_wifi = pd.read_csv("captures/0.day2_20M/user_wifi.csv", index_col='No.')

    # print(user_lifi.keys())
    # print("LiFi The measurement took {} sec".format(user_lifi['Time'].iloc[-1] - user_lifi['Time'].iloc[0]))
    # print("WiFi The measurement took {} sec".format(user_wifi['Time'].iloc[-1] - user_wifi['Time'].iloc[0]))

    # only TCP
    user_lifi = user_lifi[user_lifi.Prtcl == 'TCP']
    user_wifi = user_wifi[user_wifi.Prtcl == 'TCP']
    # only 172.16.1.1 and 172.16.1.2 IP addresses
    user_lifi = user_lifi.loc[user_lifi['Src'].isin(ip_addresses)]
    user_lifi = user_lifi.loc[user_lifi['Dst'].isin(ip_addresses)]
    user_wifi = user_wifi.loc[user_wifi['Src'].isin(ip_addresses)]
    user_wifi = user_wifi.loc[user_wifi['Dst'].isin(ip_addresses)]
    # user_lifi.drop(columns=['Info'])
    # user_wifi.drop(columns=['Info'])

    mean_rtt_to_ack_lifi = np.mean(user_lifi['RTT to ACK'])
    mean_rtt_to_ack_wifi = np.mean(user_wifi['RTT to ACK'])
    # print("LiFi mean RTT to ACK {} std {}".format(mean_rtt_to_ack_lifi, np.std(user_lifi['RTT to ACK'])))
    # print("WiFi mean RTT to ACK {} std {}".format(mean_rtt_to_ack_wifi, np.std(user_wifi['RTT to ACK'])))
    #
    time_user_lifi = list(user_lifi["Time"])
    time_user_wifi = list(user_wifi["Time"])
    # print("LiFi: number of packets: {}".format(len(time_user_lifi)))
    # print("WiFi: number of packets: {}".format(len(time_user_wifi)))

    for time in [time_user_lifi, time_user_wifi]:
        # print("******")
        previous = time[0]
        handovers = []
        for t in time:
            if abs(t-previous) > how_often_switch -1 :
                # print(t, previous)
                handovers.append(previous)
                handovers.append(t)
            previous = t
        if time == time_user_lifi:
            handovers_lifi = handovers
        elif time == time_user_wifi:
            handovers_wifi = handovers

    # print("LiFi: number of handovers {}".format(len(handovers_lifi)))
    # print("WiFi: number of handovers {}".format(len(handovers_wifi)))
    # fixme: adjust the sequences to match
    if bw in [10, 20, 201]:
        handovers_wifi = handovers_wifi[0:]
        handovers_lifi = handovers_lifi[1:]
    elif bw == 101:
        handovers_wifi = handovers_wifi[1:]
        handovers_lifi = handovers_lifi[0:]
    # print(handovers_wifi)
    # print(handovers_lifi)
    # print("LiFi interesting time stamps")
    # print(user_lifi[['Time', 'Info']].loc[user_lifi['Time'].isin(handovers_lifi)])
    # print("WiFi interesting time stamps")
    # print(user_wifi[['Time', 'Info']].loc[user_wifi['Time'].isin(handovers_wifi)])

    indexes_wifi = user_wifi.index[user_wifi['Time'].isin(handovers_wifi)]
    indexes_lifi = user_lifi.index[user_lifi['Time'].isin(handovers_lifi)]

    # print("Indexes WiFi")
    # print(list(indexes_wifi))
    # print("Indexes LiFi")
    # print(list(indexes_lifi))

    delays_lifi_wifi = []
    delays_wifi_lifi = []
    count_lifi_wifi = 0
    count_wifi_lifi = 0
    iterate_over = min(len(handovers_lifi), len(handovers_wifi))
    for i in range(iterate_over):
        if i % 2 == 0:
            delay = handovers_lifi[i] - handovers_wifi[i]
            if delay > 0:
                count_wifi_lifi += 1
            delays_wifi_lifi.append(delay)
            # print("WiFi {} {} -> LiFi {} {}: {}".format(indexes_wifi[i], handovers_wifi[i],  indexes_lifi[i],
            #                                             handovers_lifi[i], delay))
        else:
            delay = handovers_wifi[i] - handovers_lifi[i]
            if delay > 0:
                count_lifi_wifi += 1
            delays_lifi_wifi.append(delay)
            # print("LiFi {} {} -> WiFi {} {}: {}".format(indexes_lifi[i], handovers_lifi[i],  indexes_wifi[i],
            #                                             handovers_wifi[i],  delay))
    print("**************************")
    if bw in [10, 101]:
        print("Bandwidth is 10 Mbps")
    elif bw in [20, 201]:
        print("Bandwidth is 20 Mbps")
    else:
        assert 0, 'No such BW'
    print("WiFi->LiFi")
    print("mean = {}, std = {}".format(round(np.mean(delays_wifi_lifi), 4), round(np.std(delays_wifi_lifi), 4)))
    # print("Number of positive overheads {} out of {}".format(count_wifi_lifi, len(delays_wifi_lifi),))

    print("LiFi -> WiFi")
    print("mean = {}, std = {}".format(round(np.mean(delays_lifi_wifi), 4), round(np.std(delays_lifi_wifi), 4)))
    # print("Number of positive overheads {} out of {}".format(count_lifi_wifi, len(delays_lifi_wifi),))

    # print("Maximum delay {} {}".format(max(delays_lifi_wifi), max(delays_wifi_lifi)))

    # bins = int(np.sqrt(len(delays_wifi_lifi)))
    # print("Number of bins {} for {} samples ".format(bins, len(delays_wifi_lifi)))
    # plt.hist(delays_wifi_lifi, bins)
    # plt.xlabel("WiFi -> LiFi overhead (sec) ", fontsize=12)
    # plt.tick_params(labelsize=15)
    # plt.savefig("histogram_overhead_wifi_lifi_{}.png".format(bins))
    #
    # plt.cla()
    # plt.hist(delays_lifi_wifi, bins)
    # plt.xlabel("LiFi -> WiFi overhead (sec) ", fontsize=12)
    # plt.tick_params(labelsize=15)
    # plt.savefig("histogram_overhead_lifi_wifi_{}.png".format(bins))
    for i in range(len(delays_lifi_wifi)):
        delays_lifi_wifi[i] *= 1000
    for i in range(len(delays_wifi_lifi)):
        delays_wifi_lifi[i] *= 1000
    plt.errorbar(count, np.mean(delays_wifi_lifi), yerr=np.std(delays_wifi_lifi), color=color, alpha=1, capsize=8, fmt='o',
                 elinewidth=3, ls=ls)
    count += 1
    plt.errorbar(count, np.mean(delays_lifi_wifi), yerr=np.std(delays_lifi_wifi), color=color, alpha=1, capsize=8, fmt='o',
                 elinewidth=3, ls=ls)
    count += 1

folder = 'captures'
try:
    os.mkdir(folder)
except:
    pass
os.chdir(folder)
plt.grid()
plt.xticks(labels=['W->L', 'L->W', 'W->L', 'L->W', 'W->L', 'L->W', 'W->L', 'L->W'], ticks=[0, 1, 2, 3, 4, 5, 6, 7])
plt.tick_params(labelsize=9)
plt.ylabel("Overhead (ms)", fontsize=15)
plt.xlabel("Handover type", fontsize=15)
blue = mpatches.Patch(color='blue', label='10 Mbps')
green = mpatches.Patch(color='green', label='20 Mbps')
plt.legend(handles=[blue, green])
plt.axvspan(-1, 3.5, facecolor='yellow', alpha=0.5)
plt.xlim(-1, 7.5)
plt.text(0, -20, "Day 1", fontsize=15, color='black')
plt.text(4, -20, "Day 2", fontsize=15, color='black')
plt.savefig("overhead.png", dpi=500, bbox_inches='tight', pad_inches=0.1)

