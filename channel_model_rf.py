import numpy as np
import collections
import matplotlib.pyplot as plt
from channel import Channel


class AbstaractRFChannel(Channel):
    def __init__(self):
        super().__init__()
        # fixme: 2nd AP operates at 5 GHz
        self.f_rf = 2.4 * 10 ** 9
        self.subcarrier_list_lifi = [(self.f_rf + i * self.spacing) for i in range(-26, 27) if
                                     i not in [-21, -7, 0, 7, 21]]
        self.sigma_squared = -57
        self.h_r = 2.46
        self.P_r_db = 20
        self.subcarrier_list_rf = [(self.f_rf + i * self.spacing) for i in range(-26, 27) if
                                   i not in [-21, -7, 0, 7, 21]]
        self.snr_rate_rf_mbps = collections.OrderedDict(
            {0: 0, 1: 0, 2: 6.5, 5: 13, 9: 19.5, 11: 26, 15: 39, 18: 52, 20: 58.5, 26: 65})


class RFChannelModel(AbstaractRFChannel):
    def __init__(self):
        super().__init__()
        # todo: add scenario as input
        # if scenario == 'plane':
        #     self.h = 1
        # else:
        #     self.h = 3
        self.h = 2

    def get_L(self, z):
        d = np.sqrt(z**2 + self.h**2)
        X_sf = 3
        d_bp = 10
        if d < d_bp:
            L_fs_d = 20*np.log10(d) + 20*np.log10(self.f_rf) - 147.5
            L = L_fs_d + X_sf
        else:
            L_fs_dbp = 20*np.log10(d_bp) + 20*np.log10(self.f_rf) - 147.5
            L = L_fs_dbp + 35*np.log10(d/d_bp) + X_sf
        return L

    def get_sinr_db(self, z):
        L_db = self.get_L(z)
        h_r_lin = 10 ** (self.h_r / 10)
        gain_lin = np.sqrt(10 ** (-L_db / 10)) * h_r_lin
        gain = 10 * np.log10(gain_lin)
        snr_mu_r = gain + gain + self.P_r_db - self.sigma_squared
        return snr_mu_r

    def get_adaptive_rate(self, snr_db):
        rate_mbps = 0
        for key in sorted(self.snr_rate_rf_mbps.keys()):
            if key <= snr_db:
                rate_mbps = self.snr_rate_rf_mbps[key]
            if key > snr_db:
                break
        return rate_mbps

    def snr_and_datarate_based_on_snr(self):
        for snr in range(1,30):
            if snr in self.snr_rate_rf_mbps.keys():
                continue
            else:
                self.snr_rate_rf_mbps[snr] = self.snr_rate_rf_mbps[snr-1]


def main():
    rf_channel = RFChannelModel()
    for z in range(1, 30, 1):
        snr_db = rf_channel.get_sinr_db(z)
        rate = rf_channel.get_adaptive_rate(snr_db)
        plt.scatter(snr_db, rate, color='blue')
    plt.xlabel("Distance (m)")
    plt.ylabel("Adaptive rate (Mbps)")
    plt.title("RF achievable adaptive rate")
    plt.grid()
    plt.show()


if __name__ == "__main__":
    main()
