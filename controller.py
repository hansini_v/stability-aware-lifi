import numpy as np
from utilities.logs import log


class Controller:

    def __init__(self, user_list, ap_list, lifi_channel, rf_channel, simulation, with_interference):
        self.simulation = simulation
        self.user_list = user_list
        self.ap_list = ap_list
        self.lifi_channel = lifi_channel
        self.rf_channel = rf_channel
        self.sum_rate = 0
        self.global_payoff = 0
        self.number_of_mutations_occurred = 0
        self.scheduler = None
        self.id_to_ap_mapping = {}
        self.id_to_user_mapping = {}
        self.previous_allocation = {}
        self.weight_list = [0]*20 + list(np.arange(0.1, 1.1, 0.1))
        self.with_interference = with_interference

    def get_distances_from_users_to_aps(self):
        for user in self.user_list:
            for ap in self.ap_list:
                distance = np.sqrt((user.x - ap.x) ** 2 + (user.y - ap.y) ** 2)
                user.aps_their_distance[ap] = distance
                ap.users_their_distance[user] = distance

    def get_distances_to_interfering_aps(self, user, my_ap):
        interfering_aps_dist = []
        positions_list = []
        for ap_interf in self.ap_list:
            if my_ap != ap_interf and ap_interf.type == 'lifi' and my_ap.type == 'lifi':
                interfering_aps_dist.append(ap_interf.users_their_distance[user])
                positions_dict = self.get_positions_dict(ap_interf, user)
                positions_list.append(positions_dict)
                # if len(interfering_aps_dist) > 0:
                #     print("Interfering AP")
                #     print(interfering_aps_dist)
        return interfering_aps_dist, positions_list

    def get_positions_dict(self, ap, user):
        # fixme: assume only one interfering AP
        positions_dict = {}
        positions_dict['ap_x'] = ap.x
        positions_dict['ap_y'] = ap.y
        positions_dict['user_x'] = user.x
        positions_dict['user_y'] = user.y
        return positions_dict

    def get_sinr_from_users_to_aps(self):
        for user in self.user_list:
            for ap in self.ap_list:
                distance = ap.users_their_distance[user]
                if not self.with_interference:
                    interfering_aps_dist = []
                    positions_list_interf = []
                else:
                    # print("with interference")
                    interfering_aps_dist, positions_list_interf = self.get_distances_to_interfering_aps(user, ap)
                if ap.type == 'lifi':
                    if user.blocked:
                        sinr_db = 0
                    else:
                        positions_dict_ap = self.get_positions_dict(ap, user)
                        sinr_db = self.lifi_channel.get_sinr_db(distance, interfering_aps_dist,
                                                                user.elevation_angle, positions_list_interf, positions_dict_ap)
                    if sinr_db < 0:
                        sinr_db = 0
                    user.aps_their_sinrs[ap] = sinr_db
                    # print(f"User {user.my_id} with LiFi SNR {sinr_db}")
                else:
                    snr_rf_db = self.rf_channel.get_sinr_db(distance)
                    user.aps_their_sinrs[ap] = snr_rf_db
                    ap.users_their_sinrs[user] = snr_rf_db
            # print("User {}: {}; {}".format(user.my_id, user.my_sinr_dict, user.aps_their_sinrs))

    def get_possible_aps_for_users_based_on_sinr(self):
        for user in self.user_list:
            user.possible_aps = []
            for ap in user.aps_their_sinrs:
                if user.aps_their_sinrs[ap] > 0:
                    if user.blocked and ap.type == 'lifi':
                        assert 0, 'in get possible aps'
                    user.possible_aps.append(ap)
            if len(user.possible_aps) > 1 and user.blocked is True:
                assert 0, "Blocked {} user's SNR is not zero in get_possible_aps {}".format(
                    user.blocked, user.possible_aps)

    def get_adaptive_achievable_rate(self, user, ap):
        if self.simulation.vho_cost == 0:
            handover = 1
        else:
            handover = self.add_handover_overhead(user, ap)
            if handover < 1:
                print(f"Overhead {handover}")

        try:
            if ap.type == 'lifi':
                if self.with_interference:
                    interfering_aps_dist, positions_list_interf = self.get_distances_to_interfering_aps(user, ap)
                else:
                    interfering_aps_dist =[]
                    positions_list_interf = {}
                z = user.aps_their_distance.get(ap)
                if user.blocked:
                    #user.my_rate = 0
                    achievable_rate = 0
                else:
                    snr_db = None
                    positions_dict_ap = self.get_positions_dict(ap, user)
                    achievable_rate, _ = self.lifi_channel.get_adaptive_rate(z, interfering_aps_dist,
                                                                             user.elevation_angle, positions_list_interf,
                                                                             positions_dict_ap, snr_db)
            elif ap.type == 'wifi':
                snr = user.aps_their_sinrs[ap]
                achievable_rate = self.rf_channel.get_adaptive_rate(snr) * 10**6  # returns rate in Mbps
            else:
                raise NameError("Wrong AP type")
            return achievable_rate * handover
        except AttributeError:
            print("User {} {} wasn't allocated to any AP".format(user.x, user.y))

    def add_handover_overhead(self, user, ap):
        if user.my_id in self.previous_allocation and self.previous_allocation[user.my_id] != ap:
            if self.previous_allocation[user.my_id].type == ap.type:
                # HHO
                handover = 0.6  # 0.6
            else:
                # VHO
                handover = 0.4  # 0.4
        else:
            handover = 1
        return handover

    def get_payoff(self, user):
        if self.simulation.algorithm == 'GT_WO_DEMAND':
            print("payoff WITHOUT demand")
            return user.achievable_rate * user.my_resources
        else:
            return min(user.achievable_rate / user.demand * user.my_resources, 1)

    def get_resources_for_all_users(self):
        if self.scheduler == 'PF':
            for ap in self.ap_list:
                num_connected_to_same_ap_users = len(ap.connected_users)
                total_resources = 0
                for user in ap.connected_users:
                    user.my_resources = 1 / num_connected_to_same_ap_users
                    user.achievable_rate = self.get_adaptive_achievable_rate(user, ap)
                    user.my_payoff = self.get_payoff(user)

                    if user.achievable_rate == 0 and user.my_payoff > 0.001:
                        print("user's ach rate is zero")
                        assert 0, user.my_payoff
                    total_resources += user.my_resources
                    user.my_rate = user.achievable_rate * user.my_resources
                    # print(f"User {user.my_id} with rate {user.my_rate/10**6}")
                    if total_resources > 1.1:
                        print("Assigned resources violate: {}".format(total_resources))
                    # print("User {} with payoff {}".format(user.my_id, user.my_payoff))
        else:
            raise NotImplemented("Implement resource allocation for {} scheduler".format(self.scheduler))

    def initial_allocation(self):
        self.get_possible_aps_for_users_based_on_sinr()
        for user in self.user_list:
            user.my_ap = np.random.choice(list(user.possible_aps))
            user.my_sinr = user.aps_their_sinrs[user.my_ap]
            if user.my_ap.type == 'lifi' and user.blocked is True:
                print("In initial allocation, allocated blocked user to LiFi")
                assert 0
            user.my_ap.connected_users.append(user)

    def find_ap_with_max_achievable_rate_for_each_user(self, user):
        # Select based on the max achievable RATE
        if user.blocked:
            achievable_lifi_rate = 0
        else:
            self.lifi_channel.snr_and_datarate_based_on_snr()
            achievable_lifi_rate = self.lifi_channel.lifi_snr_rate_mbps.get(int(user.best_lifi_sinr_and_ap[0]),
                                self.lifi_channel.lifi_snr_rate_mbps[max(self.lifi_channel.lifi_snr_rate_mbps.keys())])
        achievable_rf_rate = self.rf_channel.snr_rate_rf_mbps.get(int(user.best_wifi_snr_and_ap[0]),
                                        self.rf_channel.snr_rate_rf_mbps[max(self.rf_channel.snr_rate_rf_mbps.keys())])

        msg = "Achievable Rate: LiFi {} Mbps,  RF {} Mbps".format(
            achievable_lifi_rate, achievable_rf_rate)
        log(type='info', event='controller', msg=msg)
        num_connected_users_with_me_wifi = len(user.best_wifi_snr_and_ap[1].connected_users) + 1
        rf_rate = achievable_rf_rate / num_connected_users_with_me_wifi

        lifi_rate = 0
        if user.best_lifi_sinr_and_ap != [0]:
            num_connected_users_with_me_lifi = len(user.best_lifi_sinr_and_ap[1].connected_users) + 1
            lifi_rate = achievable_lifi_rate / num_connected_users_with_me_lifi
        if lifi_rate > rf_rate:
            if len(user.best_lifi_sinr_and_ap[1].connected_users) < 8:
                user.my_sinr = user.best_lifi_sinr_and_ap[0]
                user.my_ap = user.best_lifi_sinr_and_ap[1]
                user.my_ap.connected_users.append(user)
                msg = "Id {} LiFi: {} Mbps ---- RF {} Mbps. Connected to LiFi".format(
                    user.my_id, int(lifi_rate), int(rf_rate))
                log(type='info', event='controller', msg=msg)
                return
        else:
            msg = "Id {} LiFi: {} Mbps ---- RF {} Mbps. Connected to RF".format(
                user.my_id, int(lifi_rate), int(rf_rate))
            log(type='info', event='controller', msg=msg)

        user.my_sinr = user.best_wifi_snr_and_ap[0]
        user.my_ap = user.best_wifi_snr_and_ap[1]
        user.my_ap.connected_users.append(user)

    def get_average_payoffs(self):
        global_payoff = 0
        for ap in self.ap_list:
            ap.average_payoff = 0
            for user in ap.connected_users:
                ap.average_payoff += user.my_payoff
            try:
                ap.average_payoff = ap.average_payoff / len(ap.connected_users)
            except ZeroDivisionError:
                ap.average_payoff = 1
            # print("Ap {}: {}".format(ap.my_id, ap.average_payoff))
            global_payoff += len(ap.connected_users) * ap.average_payoff
        self.global_payoff = global_payoff / len(self.user_list)
        # print("Global payoff {}".format(self.global_payoff))

    def decide_to_mutate_user_or_not(self, user):
        if user.my_payoff < self.global_payoff:
            user.mutation_prob = 1 - user.my_payoff / self.global_payoff
        else:
            user.mutation_prob = 0
        delta = np.random.uniform(0, 1, 1)
        if user.my_rate < 1 and user.mutation_prob < 1 and user.my_ap.type == 'lifi':
            msg = "{}, rate: {}, achiev rate: {}, payoff: {}, global payoff: {}, mutation prob: {}, user blocked: {}, " \
                  "user: {}".format(user.my_ap.type, user.my_rate, user.achievable_rate, user.my_payoff,
                                    self.global_payoff, user.mutation_prob, user.blocked, user.my_id)
            print(msg)
            assert 0

        if delta < user.mutation_prob:
            return True
        else:
            return False

    def mutate_user(self, user):
        if self.scheduler == 'PF':
            max_estimated_payoff = 0
            new_ap = None
            for ap in user.possible_aps:
                if user.blocked and ap.type == 'lifi':
                    achievable_rate = 0
                else:
                    achievable_rate = self.get_adaptive_achievable_rate(user, ap)
                if self.simulation.algorithm == 'GT_WO_DEMAND':
                    estimated_payoff = achievable_rate / (1 + len(ap.connected_users))
                else:
                    estimated_payoff = min(achievable_rate / user.demand / (1 + len(ap.connected_users)), 1.0)
                if max_estimated_payoff < estimated_payoff:
                    max_estimated_payoff = estimated_payoff
                    new_ap = ap
                    user.achievable_rate = achievable_rate
        else:
            assert 0, 'This scheduler is not implemented'

        if new_ap and new_ap != user.my_ap:
            print("Mutated {}".format(user.my_id))
            user.my_ap.connected_users.remove(user)
            user.my_ap = new_ap
            user.my_sinr = user.aps_their_sinrs[user.my_ap]
            # added: blocked users do not consume resources
            user.my_ap.connected_users.append(user)
            self.number_of_mutations_occurred += 1
        if user.blocked and user.my_ap.type == 'lifi':
            assert 0, 'in mutate blocked user to lifi'

    def ensure_resouce_constraint(self):
        for ap in self.ap_list:
            ap_rate = 0
            for user in ap.connected_users:
                ap_rate += user.my_rate
            if ap.type == 'lifi':
                max_rate = 43251441
            else:
                max_rate = 65 * 10**6
            if ap_rate > max_rate * 1.1:
                print("Violated constraint and allocated {} more to AP type {}".format(
                    ap_rate - 43251440, ap.type))
                assert 0

    def compute_fairness(self):
        sum_rate = 0
        sum_squared_rate = 0
        n = len(self.user_list)
        for user in self.user_list:
            sum_rate += user.my_rate
            sum_squared_rate += user.my_rate**2
        I = sum_rate ** 2 / n / sum_squared_rate
        return I

