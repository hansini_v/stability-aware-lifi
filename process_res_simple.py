import os
import json
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import scipy.stats


# Mean and std, as well as plots are saved in results.json in the folder res_plots

def plot_lables(fig_num):
    plt.figure(fig_num)
    plt.grid()
    # plt.xticks(labels=self.labels, ticks=list(range(-1, len(self.labels) - 1)))
    plt.tick_params(labelsize=10, pad=0.1)
    blue = mpatches.Patch(color='blue', label='LB')
    green = mpatches.Patch(color='green', label='Handoff-aware')
    red = mpatches.Patch(color='red', label='Max-SNR')
    plt.legend(handles=[red, blue, green])
    plt.gcf().set_size_inches(4, 4)
    # plt.savefig("{}_{}_b.png".format(finame, name), dpi=500, bbox_inches='tight', pad_inches=0.1)


def change_dir(scenario):
    if scenario == 'plane':
        os.chdir('sim_results/plane')
    elif scenario == 'overlap':
        os.chdir('sim_results/overlap')


def get_all_files():
    directory = os.listdir()
    files = []
    for file in directory:
        fname, ext = os.path.splitext(file)
        if ext != '.json':
            continue
        files.append(file)
    return files


def main(scenario):
    print(f"Scenario {scenario}")
    change_dir(scenario=scenario)
    files = get_all_files()
    print(f"Number of json files is {len(files)}")
    qos = []
    qos_our = []
    qos_snr = []
    tp = []
    tp_our = []
    tp_snr = []
    numhos = []
    numhos_our = []
    numhos_snr = []
    num_horizontal = []
    num_horizontal_our = []
    num_horizontal_snr = []
    means_results = {}
    tp_time = np.zeros((500,50))
    tp_our_time = np.zeros((500,50))
    tp_snr_time = np.zeros((500,50))
    iter_mine_1 = 0
    iter_mine_2 = 0
    iter_mine_3 = 0
    confidence = 0.9
    for i, file in enumerate(files):
        with open(file) as f:
            data = json.load(f)

        if data['alg'] == 'SNR':
            numhos_snr.append(data['total_num_vertical'])
            num_horizontal_snr.append(data['total_num_horizontal'])
            # fixme
            tp_snr.append(np.mean(data['sum_tp']))
            qos_snr.append(np.mean(data['qos']))
            tp_snr_time[iter_mine_3][:] = (data['sum_tp'])
            iter_mine_3 = iter_mine_3 + 1
        elif data['sim_type'] == 'with' and data['alg'] != 'SNR':
            numhos_our.append(data['total_num_vertical'])
            num_horizontal.append(data['total_num_horizontal'])
            # fixme
            tp_our.append(np.mean(data['sum_tp']))
            qos_our.append(np.mean(data['qos']))
            # tp_our.extend(data['sum_tp'])
            # qos_our.extend(data['qos'])
            tp_our_time[iter_mine_1][:] = (data['sum_tp'])
            iter_mine_1 = iter_mine_1 + 1
        else:
            numhos.append(data['total_num_vertical'])
            num_horizontal_our.append(data['total_num_horizontal'])
            # fixme
            tp.append(np.mean(data['sum_tp']))
            qos.append(np.mean(data['qos']))
            # tp.extend(data['sum_tp'])
            # qos.extend(data['qos'])
            tp_time[iter_mine_2][:] = (data['sum_tp'])
            iter_mine_2 = iter_mine_2 + 1

    mean_tp_our_time = np.mean(tp_our_time, axis=0)
    std_tp_our_time = np.std(tp_our_time, axis=0)
    mean_tp_time = np.mean(tp_time, axis=0)
    std_tp_time = np.std(tp_time, axis=0)
    mean_tp_snr_time = np.mean(tp_snr_time, axis=0)
    std_tp_snr_time = np.std(tp_snr_time, axis=0)

    numhos_mean = np.mean(numhos)
    tp_mean = np.mean(tp)
    qos_mean = np.mean(qos)

    means_results['numhos_mean'] = numhos_mean
    means_results['tp_mean'] = tp_mean
    means_results['qos_mean'] = qos_mean

    numhos_std = np.std(numhos)
    tp_std = scipy.stats.sem(tp) * scipy.stats.t.ppf((1 + confidence) / 2., len(tp)-1)
    qos_std = scipy.stats.sem(qos) * scipy.stats.t.ppf((1 + confidence) / 2., len(qos)-1)

    means_results['numhos_std'] = numhos_std
    means_results['tp_std'] = tp_std
    means_results['qos_std'] = qos_std

    numhos_our_mean = np.mean(numhos_our)
    tp_our_mean = np.mean(tp_our)
    qos_our_mean = np.mean(qos_our)

    means_results['numhos_our_mean'] = numhos_our_mean
    means_results['tp_our_mean'] = tp_our_mean
    means_results['qos_our_mean'] = qos_our_mean

    numhos_our_std = np.std(numhos_our)
    tp_our_std = scipy.stats.sem(tp_our) * scipy.stats.t.ppf((1 + confidence) / 2., len(tp_our)-1)
    qos_our_std = scipy.stats.sem(qos_our) * scipy.stats.t.ppf((1 + confidence) / 2., len(qos_our)-1)

    means_results['numhos_our_std'] = numhos_our_std
    means_results['tp_our_std'] = tp_our_std
    means_results['qos_our_std'] = qos_our_std


    num_horizontal_mean = np.mean(num_horizontal)
    num_horizontal_std = np.std(num_horizontal)

    num_horizontal_our_mean = np.std(num_horizontal_our)
    num_horizontal_our_std = np.std(num_horizontal_our)

    num_horizontal_snr_mean = np.mean(num_horizontal_snr)
    num_horizontal_snr_std = np.std(num_horizontal_snr)

    numhos_snr_mean = np.mean(numhos_snr)
    tp_snr_mean = np.mean(tp_snr)
    qos_snr_mean = np.mean(qos_snr)

    means_results['numhos_snr_mean'] = numhos_snr_mean
    means_results['tp_snr_mean'] = tp_snr_mean
    means_results['qos_snr_mean'] = qos_snr_mean

    numhos_snr_std = np.std(numhos_snr)
    tp_snr_std = scipy.stats.sem(tp_snr) * scipy.stats.t.ppf((1 + confidence) / 2., len(tp_snr)-1)
    qos_snr_std = scipy.stats.sem(tp_snr) * scipy.stats.t.ppf((1 + confidence) / 2., len(tp_snr)-1)

    means_results['numhos_snr_std'] = numhos_snr_std
    means_results['tp_snr_std'] = tp_snr_std
    means_results['qos_snr_std'] = qos_snr_std

    print(f"Num VHOs before {numhos_mean}, after {numhos_our_mean}")

    try:
        os.chdir('res_plots')
    except:
        os.mkdir('res_plots')
        os.chdir('res_plots')

    ############ number of handovers ########################
    #print("Average number of HHOs reduced by {} with mean {}%".format((num_horizontal_mean - num_horizontal_our_mean)/num_horizontal_mean*100, num_horizontal_our_mean))
    print(f"Num HHOs SNR is {num_horizontal_snr_mean}")
    ind = np.arange(2)
    width=0.2
    plt.figure(0)
    plt.bar(ind, [numhos_snr_mean, num_horizontal_snr_mean], width, color='red', label='Max-SNR')
    plt.bar(ind+width, [numhos_mean, num_horizontal_mean], width, color='blue', label='LB')
    plt.bar(ind+width*2, [numhos_our_mean, num_horizontal_our_mean], width, color='green', label='Handoff-aware')
    plt.ylabel('Number of handovers')
    plt.xticks(ind + width, ('VHO', 'HHO'))
    plt.legend()
    plt.grid()
    plt.savefig(f"handovers.pdf", dpi=1200, bbox_inches='tight', pad_inches=0.1)
    plt.show()


    ############ Sum throughput time series ########################
    plt.figure(2)
    plt.plot(np.arange(0, 25, 0.5), mean_tp_time, label='LB', color='blue', marker='o', linestyle='dashed')
    plt.plot(np.arange(0, 25, 0.5), mean_tp_our_time, label='Handoff-aware', color='green', marker='^', linestyle='dashed')
    plt.plot(np.arange(0, 25, 0.5), mean_tp_snr_time, label='Max-SNR', color='red', marker='s', linestyle='dashed')
    plt.xlabel('Time step (s)')
    plt.ylabel('Sum throughput (Mbps)')
    plt.legend()
    plt.grid()
    plt.savefig(f"Sum_tp_time.pdf", dpi=1200, bbox_inches='tight', pad_inches=0.1)
    plt.show()

    means_results_filaname = 'results.json'
    with open(means_results_filaname, 'w', encoding='utf-8') as f:
        json.dump(means_results, f, ensure_ascii=False, indent=4)


if __name__ == "__main__":
    scenario = 'overlap'
    main(scenario)