import matplotlib.pyplot as plt
import numpy as np
from utilities.plots import Plot
import time
import channel_model_lifi
import channel_model_rf
from controller import Controller
from setup import SetUp
from utilities.helper_funcs import save_results_to_json
from resource_allocation_algos import ResourceAllocationAlgorithm


class Simulation:
    def __init__(self, number_of_iterations, scenario, num_rows,
                 mobility_flag, vho_cost, varying_rx_orientation,
                  plot_or_not, sim_num, mean_elevation, speed, demands, hho_cost,
                 block_prob_one, block_prob_two, num_user_overlap, num_lifi_aps_overlap, algorithm, save_plot,
                 ap_every_num_rows, block_model):

        self.number_of_iterations = number_of_iterations
        self.scenario = scenario
        self.num_rows = num_rows
        self.mobility_flag = mobility_flag
        self.vho_cost = vho_cost
        self.varying_rx_orientation = varying_rx_orientation

        self.plot_or_not = plot_or_not
        self.sim_num = sim_num
        self.mean_elevation = mean_elevation
        self.speed = speed
        self.demands = demands
        self.hho_cost = hho_cost
        self.block_prob_one = block_prob_one
        self.block_prob_two = block_prob_two
        self.final_sum_tp = []
        self.final_qos = []
        self.final_num_unsatisfied = []
        self.final_who_made_hos = []
        self.count_num_blocked_users_list = []
        self.iteration_count = []
        self.num_total_hos_at_step = [0] * number_of_iterations
        self.check_hos_count = 0
        self.count_num_blocked_users = 0
        self.num_user_overlap = num_user_overlap
        self.num_lifi_aps_overlap = num_lifi_aps_overlap
        self.algorithm = algorithm
        self.save_plot = save_plot
        self.plotting = None
        self.flag_los_only = True
        self.ap_every_num_rows = ap_every_num_rows
        self.who_made_hos_at_each_timestep = {}
        self.block_model = block_model

    def main(self):
        setup = SetUp(self)
        setup.create_users_aps_setup(self)
        lifi_channel = channel_model_lifi.LiFiChannelModel(flag_los_only=True, scenario=self.scenario)
        rf_channel = channel_model_rf.RFChannelModel()

        controller = Controller(setup.user_list,setup.ap_list, lifi_channel, rf_channel, self, with_interference=True)
        controller.id_to_ap_mapping = setup.id_to_ap_mapping
        t0 = time.time()
        self.plotting = Plot(controller.user_list, controller.ap_list, lifi_channel, rf_channel, self.scenario,
                             self.num_rows, lifi_channel.f_sub)

        for time_step in range(self.number_of_iterations):
            self.who_made_hos_at_each_timestep[time_step] = []
            print("***\n")
            print("Time step {}".format(time_step))
            self.count_num_blocked_users = 0
            if self.varying_rx_orientation:
                setup.rx_orientation(controller, self.mean_elevation, time_step)
            else:
                setup.rx_orientation_fixed(controller, self.mean_elevation, time_step)
            if self.block_prob_one or self.block_prob_two:
                # fixme: select blockage model
                if self.block_model == 1:
                    setup.blockages_our_model_with_2_prob(controller, self.block_prob_one, self.block_prob_two)
                elif self.block_model == 3:
                    if self.speed <= 1 and time_step == 0:
                        # only in the beginning of the simulation
                        setup.blockages_with_ocupation_rate(controller, self.number_of_iterations)
                    else:
                        setup.blockages_our_model_with_2_prob(controller, self.block_prob_one, self.block_prob_two)
                elif self.block_model == 2:
                    setup.blockages_our_model_with_1_prob(controller, self.block_prob_one)
                #else:
                #    assert 0, "Wrong blockage model"

            if self.block_model == 3:
                # reduce blockage duration by one at each time step
                for user in controller.user_list:
                    user.blockage_duration -= 1
                    if user.blockage_duration == -1:
                        user.blocked = False

            #######
            resource_allocation_alg = ResourceAllocationAlgorithm(self.algorithm)
            resource_allocation_alg.run_resource_allocation_alg(time_step, controller)
            # self.get_metrics(controller)
            self.get_final_rate_and_qos(controller, time_step, 0)
            ######

        t = time.time()
        total_num_vertical_hos = 0
        total_num_horizontal_hos = 0

        fairness_index = controller.compute_fairness()
        # print("Fairness index is {}".format(fairness_index))
        for user in controller.user_list:
            total_num_vertical_hos += user.num_vho
            total_num_horizontal_hos += user.num_hho
        num_users = len(controller.user_list)
        save_results_to_json(self, num_users, total_num_vertical_hos, total_num_horizontal_hos, fairness_index)
        print(f"Results: VHO {total_num_vertical_hos}, HHO {total_num_horizontal_hos}")
        # print("Sim took {} sec \n".format(round(t - t0, 1)))

    def get_metrics(self, controller):
        if self.plot_or_not or self.save_plot:
            assert 0, "Use get_final_rate_and_qos instead of get_metrics"
        qos_list = []
        sum_tp = 0
        for user in controller.user_list:
            if user.my_id in controller.previous_allocation and controller.previous_allocation[user.my_id] != user.my_ap:
                if controller.previous_allocation[user.my_id].type == user.my_ap.type:
                    print("HHO")
                    user.num_hho += 1
                    if self.vho_cost == 0:
                        print(f"user's {user.my_id} rate is multiplied by 0.6")
                        user.my_rate = user.my_rate * 0.6  #0.6
                else:
                    print("VHO")
                    user.num_vho += 1
                    if self.vho_cost == 0:
                        print(f"user's {user.my_id} rate is multiplied by 0.4")
                        user.my_rate = user.my_rate * 0.4  #0.4
            print(f"User {user.my_id} with rate {user.my_rate/10**6}")
            controller.previous_allocation[user.my_id] = user.my_ap

            user.my_qos = round(user.my_rate / user.demand, 3)
            user.my_qos = min(user.my_qos, 1.0)

            qos_list.append(user.my_qos)
            sum_tp += user.my_rate

        self.final_qos.append(qos_list)
        self.final_sum_tp.append(round(sum_tp / 10 ** 6, 2))

    def get_final_rate_and_qos(self, controller, time_step, count):
        num_unsatisfied_users = 0
        qos_list = []
        sum_tp = 0
        who_made_hos = []
        for user in controller.user_list:
            if user.blocked:
                self.count_num_blocked_users += 1
            if user.my_id in controller.previous_allocation:
                if controller.previous_allocation[user.my_id] != user.my_ap:
                    user.made_ho = True
                    if controller.previous_allocation[user.my_id].type != user.my_ap.type:
                        user.num_vho += 1
                        self.num_total_hos_at_step[time_step] += 1
                        self.check_hos_count += 1
                        # info: VHO for W/O-HO alg
                        if self.vho_cost == 0:
                            print(f"0.4*{user.my_rate}")
                            user.my_rate = user.my_rate * 0.4  # 0.4
                            print("{}, WITHOUT VHO {} Mbps".format(user.my_id, int(user.my_rate/10**6)))
                            # assert 0
                        # else:
                            # print("WITH VHO {} MBps".format(int(user.my_rate / 10 ** 6)))
                        who_made_hos.append(user.my_id)
                        self.who_made_hos_at_each_timestep[time_step].append(user.my_id)
                    else:
                        user.num_hho += 1
                        # info: HHO for W/O-HO alg
                        # added HHO
                        if self.vho_cost == 0:
                            print(f"0.6*{user.my_rate}")
                            user.my_rate *= 0.6  # 0.6
                            print("WITHOUT HHO {} Mbps".format(int(user.my_rate/10**6)))
                            # assert 0
                        else:
                            print("WIT HHO {} Mbps".format(int(user.my_rate / 10 ** 6)))
                else:
                    user.made_ho = False

            controller.previous_allocation[user.my_id] = user.my_ap

            if (self.plot_or_not or self.save_plot) and self.scenario == 'overlap' and self.speed > 0:
                self.plotting.plot_user_mobility(controller, time_step)

            if self.mobility_flag:
                user.x = user.x_mobility[time_step]
                user.y = user.y_mobility[time_step]
            if self.algorithm == 'GT_WO_DEMAND':
                user.my_qos = round(user.my_rate)
            else:
                user.my_qos = round(user.my_rate / user.demand, 3)
                user.my_qos = min(user.my_qos, 1.0)

            if user.my_qos < 1:
                num_unsatisfied_users += 1
            qos_list.append(user.my_qos)
            sum_tp += user.my_rate

        self.count_num_blocked_users_list.append(self.count_num_blocked_users)
        self.final_qos.append(qos_list)
        self.final_sum_tp.append(round(sum_tp / 10 ** 6, 2))
        self.final_num_unsatisfied.append(num_unsatisfied_users)
        self.final_who_made_hos.append(who_made_hos)
        self.iteration_count.append(count)
        # print("Users that made handovers {}".format(who_made_hos))
        if self.plot_or_not or self.save_plot:
            self.plotting.plot_users_and_aps(time_step, params='topo')  # params='id'
            if self.scenario == 'plane':
                name = "sim_results/a_gt/plot_users_and_aps_{}_{}.png".format(
                    self.sim_num, time_step)
            elif self.scenario == 'overlap':
                name = "sim_results/b_gt/plot_users_and_aps_{}_{}.png".format(
                    self.sim_num, time_step)
            else:
                raise NameError("No such scenario name")
            plt.figure(1)
            print(f"Final QoS {self.final_qos}")
            print(len(self.final_qos))
            print(len(self.final_qos[0]))
            mean_qos = round(np.mean(self.final_qos), 2)
            print(f"mean QoS {mean_qos}")
            mean_rate = round(sum_tp / len(controller.user_list) / 10**6, 1)
            # plt.title("Alg: {}, mean QoS {}, mean rate {} Mbps".format(self.algorithm, mean_qos, mean_rate))
            if self.save_plot:
                plt.savefig(name.format(name), dpi=600)
            if self.plot_or_not:
                plt.show()

    # def debug_print_user_ap_snr_and_dist(self, controller):
    #     for user in controller.user_list:
    #         print("****")
    #         for ap in controller.ap_list:
    #             if ap.type == 'wifi':
    #                 continue
    #             print("User {} - AP {}: {} dB {} m".format(user.my_id, ap.my_id, int(user.aps_their_sinrs[ap]),
    #                                                      round(user.aps_their_distance[ap], 2)))

