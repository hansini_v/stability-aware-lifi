import matplotlib.pyplot as plt
import channel_model_lifi
import numpy as np
from utilities.helper_funcs import get_color_list
import matplotlib.patches as mpatches


class MockUser_AP:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Plot:
    def __init__(self, user_list, ap_list, lifi_channel, rf_channel, scenario, num_rows, f_sub):
        self.user_list = user_list
        self.ap_list = ap_list
        self.lifi_channel = lifi_channel
        self.rf_channel = rf_channel
        self.scenario = scenario
        self.num_rows = num_rows
        # todo: compute cell radius automatically
        if self.scenario == 'plane':
            self.lifi_cell_radius = 0.6
        elif self.scenario == 'overlap':
            self.lifi_cell_radius = 1.8
        else:
            assert 0, "Provide radius for this scenario for plotting"
        self.f_sub = f_sub
        # self.get_distance_when_snr_los_equals_zero()

    # def get_distance_when_snr_los_equals_zero(self):
    #     x = 0
    #     y = 0
    #     d = 0.5
    #     while True:
    #         lifi_channel = channel_model_lifi.LiFiChannelModel(flag_los_only=True, scenario=self.scenario)
    #         user = MockUser_AP(x,y)
    #         ap = MockUser_AP(0,0)
    #         snr_db = lifi_channel.get_sinr_db(d, f_sub=self.f_sub, interferring_aps_dist=[], shift=0, user=user, ap=ap)
    #         if snr_db <= 0:
    #             break
    #         x += 0.1
    #         y += 0.1
    #         d += 1
    #     self.lifi_cell_radius = round(d, 1)

    def plot_users_and_aps(self, time_step, params):
        ii_counter = 0
        plt.figure(1)
        plt.clf()
        if time_step == 0:
            print("Cell radius is {}".format(self.lifi_cell_radius))
        self.plot_users(params)
        self.plot_aps(ii_counter, params)
        ii_counter += 1
        if params != 'topo':
            self.plot_connections()

        if self.scenario == "plane":
            self.plot_plane()
        if params != 'topo':
            self.highlight_some_users_circle()

        plt.legend(prop={'size': 16})
        if params == 'id':
            id = mpatches.Patch(color='black', label='id')
            plt.legend(handles=[id])
        # elif params == 'topo':
        #     # lifi_ap = mpatches.Patch(color='orange', label='LiFi AP', hatch='*')
        #     # wifi_ap = mpatches.Patch(color='black', label='WiFi AP', hatch='o')
        #     # user = mpatches.Patch(color='blue', label='User', hatch='o')
        #     # plt.legend(handles=[lifi_ap, wifi_ap, user])
        #     color = np.array([['grey', 'orange', 'blue'], ] * 3).transpose()
        #     marker = ['o', 'v', 'o']
        #     label_column = ['WiFi AP', 'LiFi AP', 'User']
        #     label_row = ['1', '2', '3']
        #     rows = [mpatches.Patch(color=color[i, 0]) for i in range(3)]
        #     columns = [plt.plot([], [], marker[i], markerfacecolor='w',
        #                         markeredgecolor='k')[0] for i in range(3)]
        #
        #     plt.legend(rows + columns, label_row + label_column, loc=2)
        #

        elif params == 'topo':
            plt.xlabel("x (m)")
            plt.ylabel("y (m)")
        else:
            demand = mpatches.Patch(color='green', label='demand')
            rate = mpatches.Patch(color='black', label='rate')
            plt.legend(handles=[demand, rate])

    def highlight_some_users_circle(self):
        for user in self.user_list:
            if user.made_ho:
                circle = plt.Circle((user.x, user.y,), radius=0.15, fill=False, color='red')
                show_shape(circle)
            if user.my_rate < user.demand:
                circle = plt.Circle((user.x, user.y,), radius=0.13, fill=False, color='grey')
                show_shape(circle)

    def plot_aps(self, ii_counter, params):
        for ap in self.ap_list:
            if ap.type == 'lifi':
                plt.scatter(ap.x, ap.y, label='LiFi AP' if ii_counter == 0 else "", color='orange', s=25, marker='v')
                ii_counter += 1
                circle = plt.Circle((ap.x, ap.y,), radius=self.lifi_cell_radius, fill=False, color='orange')
                show_shape(circle)

            else:
                plt.scatter(ap.x, ap.y, label='WiFi AP', color='grey', s=70, marker='o')
            if params != 'topo':
                plt.text(ap.x - 0.05, ap.y, "{}".format(ap.my_id), fontsize=12, color='black',
                         label='id')

    def plot_users(self, params):
        ii_counter_user = 0
        for user in self.user_list:
            if user.my_ap.type == 'lifi':
                if user.blocked:
                    color = 'red'
                else:
                    color = 'blue'

            elif user.my_ap.type == 'wifi':
                color = 'grey'
            else:
                color = 'red'
            if params == 'topo':
                color = 'blue'

            self.plot_user_params(user, color, params, ii_counter_user)
            ii_counter_user += 1

    def plot_user_params(self, user, color, params, ii_counter_user):

        plt.scatter(user.x, user.y, color=color, s=14, label='LiFi AP' if ii_counter_user == 0 else "")
        if params == 'id':
            plt.text(user.x, user.y - 0.05, "{};".format(user.my_id), fontsize=12, color='black',
                     label='id')
        elif params == 'topo':
            pass
        else:
            plt.text(user.x, user.y + 0.1, "{};".format(int(user.demand / 10 ** 6)), fontsize=12, color='green',
                     label='demand')
            try:
                plt.text(user.x, user.y - 0.1, "{};".format(int(user.my_rate / 10 ** 6)), fontsize=12, label='rate')
            except AttributeError:
                print("Error. User {} {} wasn't allocated".format(user.x, user.y))

    def plot_connections(self):
        for user in self.user_list:
            if user.my_ap is None:
                print("Attention: user {} {} was NOT allocated to any AP with distances to aps {}".
                      format(user.x, user.y, user.aps_their_distance.values()))
                continue
            if user.my_ap.type == 'lifi':
                color = 'orange'
            else:
                color = 'grey'
                if user.blocked:
                    color = 'red'
            plt.plot([user.x, user.my_ap.x], [user.y, user.my_ap.y], color=color, linestyle='--')

    def plot_user_mobility(self, controller, timestep):
        colors = get_color_list()
        plt.figure(2)
        ii_counter = 0
        if timestep == 0:
            for ap in self.ap_list:
                if ap.type == 'lifi':
                    plt.scatter(ap.x, ap.y, label='LiFi AP' if ii_counter == 0 else "", color='orange', s=20, marker='v')
                    circle = plt.Circle((ap.x, ap.y,), radius=self.lifi_cell_radius, fill=False, color='yellow')
                    ax = plt.gca()
                    ax.add_patch(circle)
                    plt.axis('scaled')
                    ii_counter += 1
                else:
                    plt.scatter(ap.x, ap.y, label='WiFi AP', color='black', s=70, marker='o')

            for j in range(len(controller.user_list)):
                color = colors[j]
                for i in range(len(controller.user_list[j].x_mobility) - 1):
                    x0 = controller.user_list[j].x_mobility[i]
                    x1 = controller.user_list[j].x_mobility[i + 1]
                    y0 = controller.user_list[j].y_mobility[i]
                    y1 = controller.user_list[j].y_mobility[i + 1]
                    plt.plot([x0, x1], [y0, y1], color=color)

        for j, user in enumerate(controller.user_list):
            if user.my_ap.type == 'lifi':
                # marker = "D"
                color = 'orange'
            elif user.my_ap.type == 'wifi':
                # marker = "."
                color = 'black'
            else:
                assert 0
            # if user.blocked:
            #     color = 'red'

            if timestep == 0:
                plt.scatter(user.x, user.y, color=color, s=30, marker="*", )
            else:
                plt.scatter(user.x, user.y, color=color, s=15)

        plt.title("Movements of users", fontsize=20)

    def plot_plane(self):
        x_lines = [0.25, 0.75, 1.25, -0.25, -0.75, -1.25]
        plt.axvline(x=1.75)
        plt.axvline(x=-1.75)
        for x in x_lines:
            plt.axvline(x, linestyle='--')
        y_lines = [0.81 * x for x in range(0, self.num_rows + 1)]
        for y in y_lines:
            plt.axhline(y, linestyle='--')

        plt.xlim([-1.75, 1.75])
        plt.ylim([0, self.num_rows])

    def plot_comparion_of_lifi_and_wifi_rates_for_an_snr_value(self, ):
        for snr_db in range(0, 30):
            lifi_rate = self.lifi_channel.lifi_snr_rate_mbps.get(snr_db,
                                                                 self.lifi_channel.lifi_snr_rate_mbps[
                                                                     max(self.lifi_channel.lifi_snr_rate_mbps.keys())])
            rf_rate = self.rf_channel.snr_rate_rf_mbps.get(snr_db,
                                                           self.rf_channel.snr_rate_rf_mbps[
                                                               max(self.rf_channel.snr_rate_rf_mbps.keys())])
            plt.scatter(snr_db, lifi_rate, color='orange')
            plt.scatter(snr_db, rf_rate, color='grey')
        plt.xlabel("SNR (dB)")
        plt.ylabel("Achievable data rate (Mbps)")
        plt.title("Comparison of LiFi and WiFi achievable rates for a particular SNR")
        plt.grid()
        plt.show()

    def print_num_users(self):
        print("\n")
        count_lifi = 0
        for user in self.user_list:
            if user.my_ap.type == 'lifi':
                count_lifi += 1
        print("Total number of users {}".format(len(self.user_list)))
        print("Number of users connected to LiFi {}".format(count_lifi))

    def get_cdf_plot(self, strategy):
        rate_list = []
        for user in self.user_list:
            rate_list.append(user.my_rate / 10**6)

        plt.figure()
        n, b, _ = plt.hist(rate_list, 2 * int(np.sqrt(len(rate_list))),
                           cumulative=True)
        bins = []
        for i in range(len(b) - 1):
            bins.append(round((b[i] + b[i + 1]) / 2, 1))
        plt.figure()
        plt.plot(bins, n / len(rate_list), '-o')
        plt.grid()
        plt.xlabel("Mbps")
        plt.ylabel("CDF")
        if strategy == 'SMART':
            strategy = "Game theory Alg 2"
        plt.title("CDF of per user data rate for {} users. {}".format(len(rate_list), strategy))
        plt.show()


def show_shape(patch):
    ax = plt.gca()
    ax.add_patch(patch)
    plt.axis('scaled')
    # plt.show()
