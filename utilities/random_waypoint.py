import numpy as np
import matplotlib.pyplot as plt


class TestUser:
    def __init__(self, x, y):
        # Set initial coordinates
        self.x = x
        self.y = y
        self.xd = x
        self.yd = y
        self.x_curr = x
        self.y_curr = y
        # Save the positions
        self.x_mobility = []
        self.y_mobility = []


class RWM:
    def __init__(self, x_range, y_range, speed):
        # Set number and index of steps
        self.n_steps = None  # number of steps defines how large the hops are: e.g. 10 => 0-0.5; 5 => 0-4.6
        self.step_i = 0
        # Set waiting params
        self.waiting = True
        self.n_waiting = 3
        self.wait_i = 0
        self.x_range = x_range
        self.y_range = y_range
        self.speed = speed / 2
        if speed == -1:
            self.varying_speed = True
        else:
            self.varying_speed = False
        # self.speed = 5 / 2  # because time step is 500 ms
        # self.speed = 1.5 / 2

    def update_postion_rwm(self, user, num_hops):
        while len(user.x_mobility) < num_hops + 1:
            if not self.waiting:
                if self.step_i < self.n_steps:
                    # print("Update current coordinates in our way to the destination")
                    # Update current coordinates in our way to the destination
                    user.x_curr = user.x + (user.xd - user.x) * self.step_i / self.n_steps
                    user.y_curr = user.y + (user.yd - user.y) * self.step_i / self.n_steps
                    self.step_i += 1
                else:
                    # Set current coordinates to destination and wait
                    # print("Set current coordinates to destination and wait")
                    user.x_curr = user.xd
                    user.y_curr = user.yd
                    self.step_i = 0
                    self.waiting = True
                user.x_mobility.append(user.x_curr)
                user.y_mobility.append(user.y_curr)
                # print(user.my_id, user.x_curr, user.y_curr)
            else:
                # print("Wait")
                if self.wait_i <= self.n_waiting:
                    self.wait_i += 1
                else:
                    self.wait_i = 0
                    # print("Update initial and destination coordinates")
                    wait_i = 0
                    # Update initial and destination coordinates
                    user.x0 = user.xd
                    user.y0 = user.yd
                    # print(self.x_range)
                    # print(self.y_range)

                    user.xd = np.random.uniform(self.x_range[0]*0.9, self.x_range[1]*0.9)
                    user.yd = np.random.uniform(self.y_range[0]*0.9, self.y_range[1]*0.9)

                    # info: to get a const speed
                    if self.varying_speed:
                        self.speed = np.random.uniform(1.5, 5)
                        print("Speed {}".format(self.speed))
                    distance = np.sqrt((user.xd - user.x)**2 + (user.yd -user.y)**2)
                    hops = int(distance / self.speed) + 1
                    self.n_steps = hops

                    user.x = user.x_curr
                    user.y = user.y_curr
                    self.waiting = False


def main():
    user = TestUser(0,0)
    x_range = y_range = [-4, 2]
    rwp = RWM(x_range, y_range)
    num_hops = 10
    while len(user.x_mobility) < num_hops:
        rwp.update_postion_rwm(user)

    plt.plot(user.x_mobility, user.y_mobility)
    plt.show()
    print(user.x_mobility)
    print(user.y_mobility)

    print("User hops are:")
    for i in range(len(user.x_mobility)-1):
        print(np.sqrt((user.x_mobility[i] - user.x_mobility[i+1])**2 +
                      (user.y_mobility[i] - user.y_mobility[i+1])**2))


if __name__ == "__main__":
    main()